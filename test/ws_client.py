# https://github.com/websocket-client/websocket-client

import _thread
import time

import websocket


SERVER_HOST = '127.0.0.1'
SERVER_PORT = 8000


def on_message(ws, message):
    print('server says:', message)

def on_error(ws, error):
    print('error:', error)

def on_close(ws, close_status_code, close_msg):
    print('connection closed ')

def run(*args):
    for i in range(3):
        time.sleep(1)
        msg = 'Hello %d' % i
        print('sending `%s` to server' % msg)
        ws.send(msg)
    time.sleep(1)
    ws.close()
    print('thread terminating...')

def on_open(ws):
    _thread.start_new_thread(run, ())

if __name__ == '__main__':
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp('ws://%s:%d' % (SERVER_HOST, SERVER_PORT),
                              on_open=on_open,
                              on_message=on_message,
                              on_error=on_error,
                              on_close=on_close)

    ws.run_forever()
