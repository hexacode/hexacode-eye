from flask import Flask, jsonify, make_response, request


PORT = 8000


app = Flask(__name__)


@app.errorhandler(404)
def not_found(error):
    print('not found')
    return make_response(jsonify({'error': 'not found'}), 404)

@app.route('/map', methods=['PUT'])
def create_task():
    print(request)
    if not request.json:
        return make_response(jsonify({'error': 'not a json content'}), 400)
    if not 'map' in request.json:
        return make_response(jsonify({'error': 'missing `map` key in json content'}), 400)

    hexmap = request.json['map']
    print('Received map: %s' % hexmap)
    return '', 204


if __name__ == '__main__':
    app.run(debug=True, port=PORT)
