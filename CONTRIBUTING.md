# Contributing

## Feedback

If you use Hexacode, please send me a message. I'm just so curious about your story.

Who are you? How did you hear about this project? How do you use it? How do you think it can be improved? Was it difficult for you to build it or use it?

Let's talk (see Contact section in the readme).

## Issues

If you found a bug or want to suggest a feature, please open an issue in the [issue tracker](https://framagit.org/hexacode/hexacode-eye/-/issues), or contact me directly.

## Developping

You may want to contribute to the Hexacode Eye source code (or just hack it for you own needs), so this section is here for you.

### References

- [AprilTag user guide](https://github.com/AprilRobotics/apriltag/wiki/AprilTag-User-Guide)
- [OpenCV drawing functions](https://docs.opencv.org/4.5.1/d6/d6e/group__imgproc__draw.html)

### Developpers hints

#### Mount Raspberry Pi file system on your PC

This is useful for debugging Pi-related features, such as the Pi camera.

```
sudo mkdir /mnt/hexacode
sudo sshfs -o allow_other pi@hexacode:/home/pi /mnt/hexacode
```

You can now directly edit Raspberry Pi files from your PC, using your favorite editor. For instance:

```
codium /mnt/hexacode/hexacode-eye
```

Then, to unmount:

```
sudo umount /mnt/hexacode
```

### Analyse hexacode source code

```
.venv/bin/pylint src
.venv/bin/mypy
```

## Using an other marker type

The marker type used for Hexacode is `TagCircle21h4`, but you might want to use markers of an other type.

### a. Using a built-in marker type

AprilTag project provides builtin marker types:
- `tag36h11`, `tag25h9`, `tag16h5`: old-style markers;
- `tagStandard41h12`, `tagStandard52h13`: new-style markers, with one pixel of data on the outside;
- `tagCircle21h7`, `tagCircle49h12`: circular-shape markers;
- `tagCustom48h12`: custom-shape markers.

*Note: the first number defines the amount of pixels, the second number defines the minimal hamming distance between 2 markers.*

If you want to use one of them, it will work out of the box: just update the marker type name in your Python code (ex: `MARKER_TYPE = 'tagCircle49h12'`).

### b. Using a custom marker type

#### b.1. Get and build AprilTag generator

```
git clone https://github.com/AprilRobotics/apriltag-generation.git
cd apriltag-generation
ant
```

### b.2. Generate markers

```
mkdir <marker path>
java -cp april.jar april.tag.TagFamilyGenerator <layout> <minimal hamming distance> | tail -n +6 > ../apriltag_src/<marker_class>.java
cp src/april/tag/<marker_class>.java <marker path>/generated
ant
java -cp april.jar april.tag.TagToC april.tag.<marker_class>
java -cp april.jar april.tag.GenerateTags april.tag.<marker_class> .
mv <marker_class, first letter in lowercase>.* mosaic.png <marker path>/generated
cd ..
```

Where:
- `<marker path>`: the path of the folder containing all files related to the marker type;
- `<layout>`: marker layout, one of: `standard_x`, `classic_x`, `circle_x`, `custom_[...]` (where x is the marker size);
- `<minimal hamming distance>` is the minimal hamming distance between each marker;
- `<marker_class>` is the name of the source file defining the generated Java class dedicated to the marker type.

Example:

```
mkdir ../markers/TagCircle21h4
java -cp april.jar april.tag.TagFamilyGenerator circle_9 4 | tail -n +6 > src/april/tag/TagCircle21h4.java
cp src/april/tag/TagCircle21h4.java ../markers/TagCircle21h4/generated
ant
java -cp april.jar april.tag.GenerateTags april.tag.TagCircle21h4 ../markers/TagCircle21h4/generated
java -cp april.jar april.tag.TagToC april.tag.TagCircle21h4
mv tagCircle21h4.* mosaic.png ../markers/TagCircle21h4/generated
cd ..
```

Read [AprilTag-Generation readme](https://github.com/AprilRobotics/apriltag-generation/blob/master/README.md) for more details about its usage.

### 3. Re-install

Follow instructions in installation section, starting from [step 2.2](22-add-the-custom-marker-type-source-file).
