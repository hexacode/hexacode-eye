#!/usr/bin/env python
# TODO:
# - take photo
# - run stream
# - install lucita (install dependencies, generate sprites, clone repo, ...)
# - calibrate camera

from typing import List, Dict

import nmap


ScanResult = List[Dict[str, str]]


class Wizard:
    '''A class dedicated to a wizard used to communicate with the Raspberry Pi running Lucita.'''

    HOST_STR_FORMAT = '{ipv4:<15.15} | {names:<15.15} | {vendor:<23.23} | {mac:<17.17} | {state}'

    def __init__(self, env_file_path: str = '', ipv4: str = '', user: str = 'pi'):
        self.ipv4 = ipv4
        self.user = user
        self.local_network: ScanResult = []

        if env_file_path:
            try:
                self.load_env_file(env_file_path)
            except FileNotFoundError:
                pass

    def load_env_file(self, env_file_path: str):
        with open(env_file_path, encoding='utf-8') as env_file:
            for line in [ l for l in env_file.readlines() if not l.startswith('#')]:
                try:
                    env_key, env_value = line.split('=', 1)
                except ValueError as err:
                    raise SystemExit('Malformated line on env file:\n%s' % line) from err

                if env_key == 'IP':
                    self.ipv4 = env_value.strip()
                elif env_key == 'USER':
                    self.user = env_value.strip()

    def scan_local_network(self) -> None:
        print('Scanning local network, please wait...')

        nmps = nmap.PortScanner()
        nmps.scan(hosts='192.168.1.0/24', arguments='-sn')
        all_hosts = nmps.all_hosts()

        hosts = []
        for host in all_hosts:
            mac = nmps[host]['addresses'].get('mac', '?')
            host = {
                'names': nmps[host].hostname(),
                'vendor': nmps[host]['vendor'].get(mac, '?'),
                'ipv4': nmps[host]['addresses']['ipv4'],
                'mac': mac,
                'state': nmps[host].state()
            }

            host['str'] = self.HOST_STR_FORMAT.format(**host)
            # http://standards-oui.ieee.org/oui/oui.txt
            host['is_pi'] = host['vendor'].startswith('Raspberry Pi')
            if host['is_pi']:
                host['str'] = '\033[0;1m%s\033[0m' % host['str']

            hosts.append(host)

        self.local_network = sorted(hosts, key=lambda d: int(d['ipv4'].split('.')[3]))

    def ask_ip(self) -> None:
        header = [ 'names', 'vendor', 'ipv4', 'mac', 'state' ]

        print('\n' + self.HOST_STR_FORMAT.format(**{ k: k for k in header }))
        for host in self.local_network:
            print(host['str'])
        print()

        raspis = [host for host in self.local_network if host['is_pi']]

        if len(raspis) == 0:
            print('Can not find any Raspberry Pi in the local network.')

        elif len(raspis) == 1:
            str_raspi = '%s (%s)' % (raspis[0]['ipv4'], raspis[0]['names'])
            print('Found one Raspberry Pi in the local network: %s.' % str_raspi)
            option = self.options('Use it?', [ 'y', 'n' ])
            if option == 'y':
                self.ipv4 = raspis[0]['ipv4']

        else:
            print('Found several Raspberry Pi in the local network:')
            for raspi_idx, raspi in enumerate(raspis):
                print('%d. %s (%s)' % (raspi_idx + 1, raspi['ipv4'], raspi['names']))

            options = [ str(idx + 1) for idx in range(len(raspis)) ] + ['c']
            option = self.options('Which one to use (c to cancel)?', options)
            if option != 'c':
                self.ipv4 = raspis[int(option) - 1]['ipv4']

    @classmethod
    def options(cls, question: str, options: List[str]) -> str:
        while True:
            option = input('%s [%s]: ' % (question, '/'.join(options)))
            if option in options:
                return option

if __name__ == '__main__':
    wizard = Wizard('./env')

    if not wizard.ipv4:
        wizard.scan_local_network()
        wizard.ask_ip()

    print(wizard.ipv4)
