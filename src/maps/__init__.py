from .. import types as t
from .map import Map
from ..utils import info


# pylint: disable=import-outside-toplevel
def from_config(map_config: t.Config, size: t.ImgSize) -> Map:
    info('parsing map type `%s` in order to load a map' % map_config.map_type)

    map_args = map_config.map_type.split('-')
    if map_args[0] == 'hex':
        try:
            nb_slots_edge = int(map_args[1])
        except (ValueError, IndexError):
            pass
        else:
            from .hexagon_map import HexagonalMap
            return HexagonalMap(map_config, size, nb_slots_edge)

    raise SystemExit('Error: unrecognized map type: `%s`.' % map_config.map_type)
