from abc import abstractmethod
from time import time

from .. import types as t
from .app import App
from ..painters.compo_painter import CompoPainter


class GuiApp(App):
    '''An abstract class used to manage GUI application (ie. a window app or web app)'''

    def __init__(self, config: t.Config):
        super().__init__(config)
        self.compo_painter = CompoPainter(config.compo, self.map, config.detect.marker_type)
        self.title = 'Hexacode - %s' % self.capture.name

    @abstractmethod
    def start(self) -> None:
        self.compo_painter.load()

    @abstractmethod
    def render(self) -> None:
        fps = round(1 / (time() - self.time))
        self.compo_painter.draw_stream(self.capture.image, fps)
        self.compo_painter.compose_images()

    def post_process(self) -> None:
        self.compo_painter.overlay_painter.draw(self.tile_finder)
        self.compo_painter.map_painter.draw(self.tile_finder)
