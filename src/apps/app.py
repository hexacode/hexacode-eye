from abc import abstractmethod
from time import time

from .. import types as t
from ..api_client import ApiClient
from ..tile_finder import TileFinder
from ..captures import from_config as capture_from_config
from ..maps import from_config as map_from_config


class App:
    '''
    Abstract class that manages the application, which can be GUI or headless.
    '''

    def __init__(self, config: t.Config):
        self.config = config
        self.capture = capture_from_config(config.capture)
        self.map = map_from_config(self.config.map, self.capture.size)
        self.tile_finder = TileFinder(self.config.detect, self.map)
        self.api_client = ApiClient(self.config.api)
        self.time = time()

    @abstractmethod
    def start(self) -> None:
        pass

    @abstractmethod
    def render(self) -> None:
        pass

    @abstractmethod
    def post_process(self) -> None:
        pass

    def process(self) -> None:
        self.tile_finder.find_markers(self.capture.image)

        if not self.config.api.disable_api:
            self.api_client.update_map({})

        self.post_process()

    def run(self) -> None:
        self.capture.start()
        self.start()
        frame_counter = 0

        while True:
            self.capture.shoot()
            if self.config.detect.detect_each != 0:
                frame_counter += 1
                if frame_counter % self.config.detect.detect_each == 0:
                    frame_counter = 0

                if frame_counter == 0:
                    self.process()

                self.render()
