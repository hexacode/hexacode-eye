from .app import App
from ..utils import info


class HeadlessApp(App):
    '''
    App without a GUI but continue to detect tiles and communicate with the hexacode api.
    '''

    def start(self) -> None:
        info('starting app in headless mode')

    def render(self) -> None:
        pass

    def post_process(self) -> None:
        pass
