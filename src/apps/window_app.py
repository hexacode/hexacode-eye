from sys import exit as sys_exit
from tkinter import Tk  # only used to get sreen size

import cv2

from .gui_app import GuiApp
from .. import types as t
from ..utils import info


class WindowApp(GuiApp):
    '''
    Hexacode GUI that display generated composition and handle user inputs
    '''

    def start(self) -> None:
        info('starting app in windowed mode')
        super().start()

        cv2.namedWindow(self.title, cv2.WINDOW_GUI_NORMAL)
        if self.config.gui.fullscreen:
            info('setting fullscreen mode')
            cv2.setWindowProperty(self.title, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
        else:
            cv2.resizeWindow(self.title, self.compo_painter.size[0], self.compo_painter.size[1])
            screen_size = self.get_screen_size()
            pos_x = int( screen_size[0] / 2 - self.compo_painter.size[0] / 2 )
            pos_y = int( screen_size[1] / 2 - self.compo_painter.size[1] / 2 )
            info('setting window size to %dx%d at position [%d;%d]' % (*screen_size, pos_x, pos_y))
            cv2.moveWindow(self.title, pos_x, pos_y)

    def render(self) -> None:
        super().render()
        key = cv2.waitKey(1)

        if key != -1:
            info('user pressed key %d' % key)

        # Space key
        if key == 32:
            info('updating map')
            self.process()

        # Escape key or close window button
        if key == 27 or cv2.getWindowProperty(self.title, cv2.WND_PROP_VISIBLE) < 1:
            info('user asked to exit, closing app')
            cv2.destroyAllWindows()
            sys_exit(0)

        cv2.imshow(self.title, self.compo_painter.image)

    @classmethod
    def get_screen_size(cls) -> t.ImgSize:
        root = Tk()
        return root.winfo_screenwidth(), root.winfo_screenheight()
