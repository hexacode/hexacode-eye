from typing import Tuple, List, Dict

import numpy as np
from configargparse import Namespace


Config = Namespace

Point = Tuple[int, int]
ImgSize = Tuple[int, int]

Polygon = List[Point]
Polygons = List[Polygon]

Marker = Tuple[int, Point]
Markers = Dict[str, Marker]
Lattice = Dict[str, Point]

Color = Tuple[int, int, int, int]

Image = np.ndarray
