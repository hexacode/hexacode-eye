from .. import types as t
from ..utils import info
from .capture import Capture


# pylint: disable=import-outside-toplevel
def from_config(capture_config: t.Config) -> Capture:
    info('loading the right input source based on user config')

    if capture_config.image_path:
        from .image_file_capture import ImageFileCapture
        return ImageFileCapture(capture_config)

    if capture_config.device == 'picam':
        from .picam_capture import PiCamCapture
        return PiCamCapture(capture_config)

    from .webcam_capture import WebcamCapture
    return WebcamCapture(capture_config)
