# Resources:
# https://v4l2ctl.readthedocs.io/en/latest/main_docs.html

from typing import List, Optional

import v4l2ctl

from ..utils import info
from .. import types as t


# pylint: disable=too-many-instance-attributes
class CameraInfo:
    '''Utility class used to query camera information, using v4l2ctl.'''

    def __init__(self, capture_config: t.Config):
        self.capture_config = capture_config

        self._device: Optional[v4l2ctl.V4l2Device] = None
        self._format: Optional[v4l2ctl.v4l2format.V4l2Format] = None
        self._target_size = (0, 0)
        self._nearest_size = (0, 0)
        self._device_info = ''

        self._devices: List[v4l2ctl.V4l2Device] = []
        self._devices_paths: List[str] = []
        self._devices_info = ''

    @property
    def device(self) -> v4l2ctl.V4l2Device:
        if self._device is not None:
            return self._device

        device_path = self.capture_config.device

        try:
            self._device = v4l2ctl.V4l2Device(device_path)
        except FileNotFoundError as err:
            devices = ', '.join(self.devices_path)
            raise SystemExit('Error: can not find camera device at %s. Available devices: %s' \
                % (device_path, devices)) from err

        return self._device

    @property
    def name(self) -> str:
        return self.device.name

    @property
    def path(self) -> str:
        return str(self.device.device)

    @property
    def format(self) -> v4l2ctl.v4l2format.V4l2Format:
        if self._format is not None:
            return self._format

        for d_format in self.device.formats:
            if self.get_fourcc(d_format) == self.capture_config.video_codec:
                self._format = d_format
                return self._format

        codecs = ', '.join([ self.get_fourcc(fmt) for fmt in self.device.formats ])
        raise SystemExit('Error: camera device %s does not support codec %s. Supported codecs: %s' \
            % (self.capture_config.device, self.capture_config.video_codec, codecs))

    @property
    def target_size(self) -> t.ImgSize:
        if self._target_size != (0, 0):
            return self._target_size

        try:
            width, height = (int(size) for size in self.capture_config.video_size.split('x'))
        except ValueError as err:
            raise SystemExit('Error: bad video size: %s.' % self.capture_config.video_size) from err

        self._target_size = (width, height)
        info('video target size: %dx%d' % self._target_size)
        return self._target_size

    @property
    def nearest_size(self) -> t.ImgSize:
        if self._nearest_size != (0, 0):
            return self._nearest_size

        nearest_size = (0, 0)
        nearest_score = 9999

        for size in self.get_sizes(self.format):
            score = abs(self.target_size[0] - size[0]) + abs(self.target_size[1] - size[1])
            if score == 0:
                return size

            if score < nearest_score:
                nearest_size = size
                nearest_score = score

        self._nearest_size = nearest_size
        info('video actual size: %dx%d' % self._nearest_size)
        return self._nearest_size

    @property
    def device_info(self) -> str:
        if self._device_info == '':
            self._device_info = self.get_device_info(self.device)
        return self._device_info

    @property
    def devices(self) -> List[v4l2ctl.V4l2Device]:
        if self._devices != []:
            return self._devices

        try:
            for device in v4l2ctl.V4l2Device.iter_devices():
                if v4l2ctl.V4l2Capabilities.VIDEO_CAPTURE in device.capabilities:
                    self._devices.append(device)
                else:
                    info('ignoring device %s that does not support capture' % device.device)
        except IndexError:
            info('index error when iterating over devices')

        return self._devices

    @property
    def devices_path(self) -> List[str]:
        if self._devices_paths == []:
            self._devices_paths = [ str(device.device) for device in self.devices ]

        return self._devices_paths

    @property
    def devices_info(self) -> str:
        return '\n'.join([ self.get_device_info(device) for device in reversed(self.devices) ])

    @classmethod
    def get_device_info(cls, device: v4l2ctl.V4l2Device) -> str:
        device_info = '%s (%s)' % (str(device.device), device.name)

        for d_format in device.formats:
            device_info += '\n  %s (%s)\n' % (cls.get_fourcc(d_format), d_format.description)
            device_info += cls.get_str_sizes(d_format)

        return device_info

    # pylint: disable=protected-access
    @classmethod
    def get_fourcc(cls, d_format: v4l2ctl.v4l2format.V4l2Format) -> str:
        return d_format._fmt_desc.pixelformat.to_bytes(4, 'little').decode()

    @classmethod
    def get_sizes(cls, d_format: v4l2ctl.v4l2format.V4l2Format) -> List[t.ImgSize]:
        sizes = []
        for size in d_format.sizes():
            width = size.width[1] if size.type.name == 'STEPWISE' else size.width
            height = size.height[1] if size.type.name == 'STEPWISE' else size.height
            sizes.append( (width, height) )
        return sizes

    @classmethod
    def get_str_sizes(cls, d_format: v4l2ctl.v4l2format.V4l2Format) -> str:
        str_sizes = []
        for width, height in cls.get_sizes(d_format):
            pgcd = cls.pgcd(width, height)
            str_sizes.append('    %dx%d (%d/%d)' % (width, height, width / pgcd, height / pgcd))
        return '\n'.join(str_sizes)

    @classmethod
    def pgcd(cls, int_a: int, int_b: int) -> int:
        return int_a if int_b == 0 else cls.pgcd(int_b, int_a % int_b)
