from __future__ import annotations
from abc import abstractmethod

from .. import types as t


class Capture:
    '''
    abstract class that handle input sources, such as camera stream or file image.
    '''

    def __init__(self, input_config: t.Config):
        self.input_config = input_config
        self.image: t.Image

    @property
    def name(self) -> str:
        return self.get_name()

    @abstractmethod
    def get_name(self) -> str:
        pass

    @property
    def size(self) -> t.ImgSize:
        return self.get_size()

    @abstractmethod
    def get_size(self) -> t.ImgSize:
        pass

    @abstractmethod
    def start(self) -> None:
        pass

    @abstractmethod
    def shoot(self) -> None:
        pass
