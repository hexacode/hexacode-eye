from .capture import Capture
from ..utils import info
from .. import types as t


class ImageFileCapture(Capture):
    '''
    Input source dedicated to get image files.
    '''

    def get_name(self) -> str:
        return self.input_config.image_path

    def get_size(self) -> t.ImgSize:
        return (0, 0)

    def start(self) -> None:
        info('starting to load image file')

    def shoot(self) -> None:
        pass
