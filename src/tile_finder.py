import math

import cv2
from apriltag import apriltag

from . import types as t
from .utils import info
from .maps.map import Map


class TileFinder:
    '''
    Find and analyse tiles in an image.
    '''

    EPSILON = 0.3

    def __init__(self, detect_config: t.Config, tile_map: Map):
        self.detect_config = detect_config
        self.map = tile_map

        self.markers: t.Markers = {}
        self.good_polygons: t.Polygons = []
        self.too_far_polygons: t.Polygons = []
        self.not_squared_polygons: t.Polygons = []

    # TODO: now we have self.map (and so, self.map.size), scale all positions
    # TODO: split find markers and filtering against lattice in 2 methods
    #       so constructor takes only config and filter_marker() takes the map

    def find_markers(self, cv2_image: t.Image) -> None:
        im_gray = cv2.cvtColor(cv2_image, cv2.COLOR_BGR2GRAY)
        detections = apriltag(self.detect_config.marker_type).detect(im_gray)

        self.markers = {}
        self.good_polygons = []
        self.too_far_polygons = []
        self.not_squared_polygons = []

        for detection in detections:
            polygon = detection['lb-rb-rt-lt']

            if TileFinder.is_valid(polygon):
                for slot_id, slot_pos in self.map.lattice.items():
                    distance_from_center = self.get_distance(detection['center'], slot_pos)
                    if distance_from_center < self.detect_config.max_marker_distance:
                        self.good_polygons.append(polygon)
                        self.markers[slot_id] = detection['id'], detection['center']
                        break
                self.too_far_polygons.append(polygon)
            else:
                self.not_squared_polygons.append(polygon)
        info('found %s (m=%d, p=%d, f=%d, n=%d)' \
             % (', '.join([ '%d@%s' % (m[0], s) for s, m in self.markers.items() ]),
                len(self.markers), len(self.good_polygons),
                len(self.too_far_polygons), len(self.not_squared_polygons)) )

    @classmethod
    def is_valid(cls, polygon: t.Polygon) -> bool:
        # x_values, y_values = list(zip(*polygon))
        # delta_x = max(x_values) - min(x_values)
        # delta_y = max(y_values) - min(y_values)

        # if     delta_x < edge_length - self.EPSILON \
        #     or delta_x > diag_length - self.EPSILON \
        #     or delta_y < edge_length - self.EPSILON \
        #     or delta_y > diag_length - self.EPSILON:
        #     return False
        # return True
        bottom_left, bottom_right, top_right, top_left = polygon

        len_edges = [ TileFinder.get_distance(bottom_left, bottom_right),
                      TileFinder.get_distance(bottom_right, top_right),
                      TileFinder.get_distance(top_right, top_left),
                      TileFinder.get_distance(bottom_left, top_left)]
        len_diags = [ TileFinder.get_distance(bottom_left, top_right),
                      TileFinder.get_distance(bottom_right, top_left) ]

        edges_are_equal = ( max(len_edges) / (min(len_edges) + 0.1) ) < (1 + cls.EPSILON)
        diags_are_equal = ( max(len_diags) / (min(len_diags) + 0.1) ) < (1 + cls.EPSILON)

        return edges_are_equal and diags_are_equal

    @staticmethod
    def get_distance(point_a: t.Point, point_b: t.Point):
        return math.sqrt(abs(point_a[0] - point_b[0]) ** 2 + abs(point_a[1] - point_b[1]) ** 2)
