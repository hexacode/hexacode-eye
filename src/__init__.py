from . import types as t
from .utils import info
from .config_parser import ConfigParser
from .captures.camera_info import CameraInfo
from .apps import from_config as app_from_config


def run() -> None:
    config = ConfigParser()
    config.parse()

    if config.main.list_cameras:
        print(CameraInfo(config.capture).devices_info)
    else:
        app = app_from_config(config)
        app.run()
