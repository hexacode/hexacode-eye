from __future__ import annotations
from sys import argv

import configargparse as cap

from . import utils


# pylint: disable=too-many-instance-attributes
class ConfigParser:
    '''
    Parse CLI arguments and configuration file.
    '''

    def __init__(self):
        utils.info('initializing config parser')
        self.main: cap.Namespace
        self.gui: cap.Namespace
        self.compo: cap.Namespace
        self.api: cap.Namespace
        self.capture: cap.Namespace
        self.detect: cap.Namespace
        self.map: cap.Namespace

        formatter = lambda prog: cap.RawTextHelpFormatter(prog, max_help_position=10)

        self.parser = cap.ArgParser(
            description='Hexacode Eye - Find tiles from hexacode map images in a webcam stream.',
            usage='%s [--headless] [--api-url=API_URL] [other options] [image path]' % argv[0],
            formatter_class=formatter,
            add_config_file_help=False,
            ignore_unknown_config_file_keys=True,
            config_file_parser_class=cap.YAMLConfigFileParser,
            default_config_files=[ utils.DEFAULT_CONFIG_PATH ])

        self.add_main_config()
        self.add_gui_config()
        self.add_compo_config()
        self.add_api_config()
        self.add_capture_config()
        self.add_detect_config()
        self.add_map_config()

    def parse(self) -> ConfigParser:
        # pylint: disable=protected-access
        utils.info('parsing configuration from CLI args and config file')

        config = self.parser.parse_args()

        arg_groups = {}
        for group in self.parser._action_groups:
            group_dict = { a.dest: getattr(config, a.dest, None) for a in group._group_actions }
            arg_groups[group.title] = cap.Namespace(**group_dict)

        self.main = arg_groups['Global']
        self.gui = arg_groups['GUI']
        self.compo = arg_groups['Composition']
        self.api = arg_groups['API']
        self.capture = arg_groups['Capture']
        self.detect = arg_groups['Detection']
        self.map = arg_groups['Map']

        return self

    def add_main_config(self) -> None:
        group = self.parser.add_argument_group('Global')

        group.add('-C', '--config', is_config_file=True, default=utils.CONFIG_DEFAULT_PATH,
                        help='path of a file to load the config from')

        group.add('-v', '--verbose', action='store_true',
                        help='enable verbose mode')

        group.add('-l', '--list-cameras', action='store_true',
                        help='list avaible cameras and their information')

    def add_gui_config(self) -> None:
        group = self.parser.add_argument_group('GUI')

        group.add('-H', '--headless', action='store_true',
                  help='disable the GUI, but still communicate marker positions via the api')

        group.add('-W', '--web', action='store_true',
                  help='use the web app instead the windowed app')

        group.add('-f', '--fullscreen', action='store_true',
                  help='open window in fullscreen (windowed mode only)')

    def add_compo_config(self) -> None:
        group = self.parser.add_argument_group('Composition')

        group.add('-w', '--image-width', type=float,
                  help='width of rendered images: absolute if >1, otherwise scale from input image')

        group.add('-c', '--hide-capture', action='store_true',
                  help='don\'t display video capture')

        group.add('-m', '--hide-map', action='store_true',
                  help='don\'t display generated map')

        group.add('--horizontal', action='store_true',
                  help='compose images horizontally instead vertically')

        group.add('-o', '--overlay',
                  help='list of elements that should appear on overlay image\n' \
                      + '(g=grid, l=links, p=good polygons, f=too far polygons, ' \
                      + 'n=not squared polygons, -=disable overlay)')

    def add_api_config(self) -> None:
        group = self.parser.add_argument_group('API')

        group.add('--disable-api', action='store_true',
                  help='disable communication with hexacode api')

        group.add('-u', '--api-url',
                  help='url of the hexacode api')

    def add_capture_config(self) -> None:
        group = self.parser.add_argument_group('Capture')

        group.add('-i', '--image-path',
                  help='path of an image to analyse instead a webcam stream')

        group.add('-d', '--device',
                  help='video device identifier (ie. `picam`, `/dev/video0`, `/dev/video1`, ...)')

        group.add('-r', '--video-size',
                  help='requested video capture resolution (with <x>x<y> notation, in px)')

        group.add('--video-codec',
                  help='four-character code of the codec used to decode video (ie. MJPG or JPEG)')

        group.add('--mirror', action='store_true',
                  help='mirror input image')

        group.add('--flip', type=int,
                  help='flip input image - one of: [ 0 (0°), 1 (90°), 2 (180°), 3 (-90°) ]')

    def add_detect_config(self) -> None:
        group = self.parser.add_argument_group('Detection')

        group.add('--max-marker-size-diff', type=int,
                  help='max difference between expected slot size and marker size (in px)')

        group.add('--max-marker-distance', type=int,
                  help='max distance between expected slot position and marker position (in px)')

        group.add('--detect-each', type=int,
                  help='number of frames to read between two detections (O to disable auto-detect)')

        group.add('--marker-type',
                  help='marker type (see readme)')

    def add_map_config(self) -> None:
        group = self.parser.add_argument_group('Map')

        group.add('--map-type',
                  help='type of map used to associate a tile position to a slot')

        group.add('--map-offset',
                  help='offset applied to the map position (with <x>x<y> notation, in px)')

        group.add('--map-scale', type=int,
                  help='scale ratio between capture image and map')

        group.add('--slot-size', type=float,
                  help='slot size (in %% of image width)')
