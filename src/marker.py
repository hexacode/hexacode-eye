from enum import Enum


class Marker(Enum):
    '''
    Enumeration of markers with data about each.
    '''

    def __init__(self, label: str, size: int, nb_bits: int, amount: int):
        self.label = label
        self.class_name = label[:1].upper() + label[1:]
        self.size = size
        self.nb_bits = nb_bits
        self.amount = amount

    CIRCLE_21H4 = ('tagCircle21h4', 9, 21, 1792)

    @classmethod
    def from_label(cls, label):
        for marker in cls:
            if label == marker.label:
                return marker
        raise SystemExit('Error: unsuported marker `%s`.' % label)
