from typing import cast, Tuple
# import multiprocessing

import cv2

from .. import utils
from .. import types as t
from ..tile_finder import TileFinder
from .img_utils import ImgUtils
from .painter import Painter
from ..maps.map import Map


class MapPainter(Painter):
    '''
    Draw shapes related to tile detection.
    '''

    MAP_BG_COLOR = '#AAA'
    MAP_SLOTS_COLOR = '#888'

    def __init__(self, tile_map: Map, scale: float):
        utils.info('initializing map painter')
        super().__init__(tile_map, scale)
        self.atlas_image = cv2.imread(utils.ATLAS_IMAGE_PATH, flags=cv2.IMREAD_UNCHANGED)
        self.sprite_size = (0, 0)

    def load_sprite(self, line: str) -> Tuple[int, t.Image]:
        marker_id = int(line.split('\t')[1].split('_')[0])
        sprite_name = line.split('\t')[1].split('_')[1].strip()
        sprite_pos = cast(Tuple[int, int], tuple( int(n) for n in line.split('\t')[0].split(';') ))

        utils.info('loading sprite %d (%s) from atlas at pos [%d;%d]' \
            % (marker_id, sprite_name, *sprite_pos) )
        return marker_id, ImgUtils.crop(self.atlas_image, sprite_pos, self.sprite_size)

    def load_sprites(self, *args) -> None:
        with open(utils.ATLAS_INDEX_PATH, encoding='utf-8') as atlas_index_file:
            lines = atlas_index_file.readlines()
            str_sprite_size = lines.pop(0)[1:].split('\t')[1].split('x')
            self.sprite_size = cast(Tuple[int, int], tuple( int(n) for n in str_sprite_size))
            utils.info('loading %d sprites of size %dx%d' % (len(lines), *self.sprite_size))
            self.sprites = dict(map(self.load_sprite, lines))
            # with multiprocessing.Pool() as pool:
            #     self.sprites = dict(pool.map(self.load_sprite, lines))

    def draw_background(self) -> None:
        self.background = ImgUtils.new_image(self.size, self.MAP_BG_COLOR)
        self.draw_bg_slots()

    def draw_bg_slots(self) -> None:
        utils.info('drawing background slots')
        color = ImgUtils.hex_to_bgra(self.MAP_SLOTS_COLOR)
        if color[3] == 255:
            return

        for slot_pos in self.map.lattice.values():
            cv2.circle(self.background, self.scale(slot_pos), self.map.slot_radius,
                       color, -1, self.LINE_STYLE)

    def draw(self, tile_finder: TileFinder) -> None:
        self.image = self.background.copy()
        self.draw_sprites(tile_finder.markers)
