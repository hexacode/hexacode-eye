from typing import Dict, List

import cv2
import numpy as np

from .. import utils
from .. import types as t
from ..tile_finder import TileFinder
from ..maps.map import Map
from .painter import Painter
from .marker_painter import MarkerPainter
from .img_utils import ImgUtils


class OverlayPainter(Painter):
    '''
    Draw shapes related to tile detection.
    '''

    CROSS_SIZE = 5
    STATUS_BAR_POS = (10, 10)

    STATUS_BAR_COLOR = '#FFFF'
    LATTICE_COLOR = '#0F08'
    LINKS_COLOR = '#F0F8'
    GOOD_POLYGONS_COLOR = '#0F0'
    TOO_FAR_POLYGONS_COLORS = '#0FF8'
    NOT_SQUARED_POLYGONS_COLOR = '#F008'
    BACKGROUND_COLOR = '#0000'

    def __init__(self, tile_map: Map, scale: float, overlay_config: str):
        super().__init__(tile_map, scale)
        utils.info('initializing overlay painter')
        self.overlay_config = overlay_config
        self.status_bar_text = ''

    def load_sprites(self, *args) -> None:
        if 'm' not in self.overlay_config:
            return

        utils.info('loading overlay sprites')
        marker_painter: MarkerPainter = args[0]

        self.sprites = {}
        with open(utils.ATLAS_INDEX_PATH, encoding='utf-8') as atlas_index_file:
            for line in atlas_index_file.readlines():
                try:
                    if line.startswith('#'):
                        continue
                    marker_id = int(line.split('\t')[1].split('_')[0])
                    self.sprites[marker_id] = marker_painter.get_sprite(marker_id)
                    utils.info('loaded sprite %d' % marker_id)
                except ValueError:
                    utils.info('malformated line in atlas index file: `%s`, skipping.' % line)

    def draw_background(self) -> None:
        utils.info('drawing overlay background')
        self.background = ImgUtils.new_image(self.size)
        if 'g' in self.overlay_config:
            self.draw_bg_lattice()

    def draw_bg_lattice(self) -> None:
        utils.info('drawing overlay background grid')
        color = ImgUtils.hex_to_bgra(self.LATTICE_COLOR)

        for slot_pos in self.map.lattice.values():
            self.draw_bg_cross(self.scale(slot_pos), color)

    def draw_bg_cross(self, pos: t.Point, color: t.Color) -> None:
        half_size = int( (self.CROSS_SIZE - 1) / 2)

        cv2.line(self.background, (pos[0], pos[1] - half_size), (pos[0], pos[1] + half_size),
                 color, self.LINE_THICKNESS)
        cv2.line(self.background, (pos[0] - half_size, pos[1]), (pos[0] + half_size, pos[1]),
                 color, self.LINE_THICKNESS)

    def draw(self, tile_finder: TileFinder) -> None:
        if '-' in self.overlay_config:
            return

        self.image = self.background.copy()
        if 'p' in self.overlay_config:
            self.draw_good_polygons(tile_finder.good_polygons)
        if 'f' in self.overlay_config:
            self.draw_too_far_polygons(tile_finder.too_far_polygons)
        if 'n' in self.overlay_config:
            self.draw_not_squared_polygons(tile_finder.not_squared_polygons)
        if 'l' in self.overlay_config:
            self.draw_links(tile_finder.markers)
        if 'm' in self.overlay_config:
            self.draw_sprites(tile_finder.markers)
        if 's' in self.overlay_config:
            self.draw_status_bar()

    def draw_marker_id(self, marker: t.Marker, radius: int, color: t.Color) -> None:
        marker_id, marker_pos = marker
        text_center = self.scale((marker_pos[0] - radius - 1, marker_pos[1] + radius - 3))

        cv2.putText(self.image, '%d' % marker_id, text_center, self.FONT,
                    self.FONT_SCALE, color, self.LINE_THICKNESS, self.LINE_STYLE)

    def draw_status_bar(self) -> None:
        color = ImgUtils.hex_to_bgra(self.STATUS_BAR_COLOR)
        text_pos = self.STATUS_BAR_POS[0], self.size[1] - self.STATUS_BAR_POS[1]

        cv2.putText(self.image, self.status_bar_text, text_pos, self.FONT,
                    self.FONT_SCALE, color, self.LINE_THICKNESS, self.LINE_STYLE)

    def draw_links(self, markers: Dict[str, t.Marker]) -> None:
        color = ImgUtils.hex_to_bgra(self.LINKS_COLOR)

        for slot_id, marker in markers.items():
            slot_pos = self.scale(self.map.lattice[slot_id])
            marker_pos = self.scale(marker[1])
            cv2.line(self.image, slot_pos, marker_pos, color, thickness=2)

    def draw_good_polygons(self, polygons: List[t.Polygon]) -> None:
        color = ImgUtils.hex_to_bgra(self.GOOD_POLYGONS_COLOR)

        for polygon in polygons:
            self.draw_polygon(polygon, color)

    def draw_too_far_polygons(self, polygons: List[t.Polygon]) -> None:
        color = ImgUtils.hex_to_bgra(self.TOO_FAR_POLYGONS_COLORS)

        for polygon in polygons:
            self.draw_polygon(polygon, color)

    def draw_not_squared_polygons(self, polygons: List[t.Polygon]) -> None:
        color = ImgUtils.hex_to_bgra(self.NOT_SQUARED_POLYGONS_COLOR)

        for polygon in polygons:
            self.draw_polygon(polygon, color)

    def draw_polygon(self, polygon: t.Polygon, color: t.Color) -> None:
        np_polygon = [ np.array([ self.scale(p) for p in polygon ], np.int32) ]
        cv2.polylines(self.image, np_polygon, True, color, self.LINE_THICKNESS, self.LINE_STYLE)
