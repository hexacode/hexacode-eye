import numpy as np

from .. import types as t


class ImgUtils:
    '''
    Utility class with only static methods for image manipulation with OpenCV.
    '''

    @staticmethod
    def crop(image: t.Image, img_pos: t.Point, img_size: t.ImgSize):
        return image[img_pos[1]:img_pos[1] + img_size[1], img_pos[0]:img_pos[0] + img_size[0]]

    @staticmethod
    def overlay(background: t.Image, foreground: t.Image) -> None:
        alpha_bg = background[:,:,3] / 255.0
        alpha_fg = foreground[:,:,3] / 255.0

        for color in range(0, 3):
            background[:,:,color] = alpha_fg * foreground[:,:,color] + \
                alpha_bg * background[:,:,color] * (1 - alpha_fg)

        background[:,:,3] = (1 - (1 - alpha_fg) * (1 - alpha_bg)) * 255

    @staticmethod
    def offset_overlay(img: t.Image, img_overlay: t.Image, offset: t.Point) -> None:
        # pylint: disable=invalid-name
        # source: https://stackoverflow.com/a/45118011/2914540

        alpha_mask = img_overlay[:, :, 3] / 255.0
        img = img[:, :, :3]
        img_overlay = img_overlay[:, :, :3]

        x1, x2 = max(0, offset[0]), min(img.shape[1], offset[0] + img_overlay.shape[1])
        y1, y2 = max(0, offset[1]), min(img.shape[0], offset[1] + img_overlay.shape[0])

        x1o, x2o = max(0, -offset[0]), min(img_overlay.shape[1], img.shape[1] - offset[0])
        y1o, y2o = max(0, -offset[1]), min(img_overlay.shape[0], img.shape[0] - offset[1])

        if y1 >= y2 or x1 >= x2 or y1o >= y2o or x1o >= x2o:
            return

        img_crop = img[y1:y2, x1:x2]
        img_overlay_crop = img_overlay[y1o:y2o, x1o:x2o]
        alpha = alpha_mask[y1o:y2o, x1o:x2o, np.newaxis]

        img_crop[:] = alpha * img_overlay_crop + (1.0 - alpha) * img_crop

    @staticmethod
    def hex_to_bgra(hex_color: str) -> t.Color:
        hcl = hex_color.lstrip('#')
        if len(hcl) in [3, 6]:
            hcl += '0' if len(hcl) == 3 else '00'

        if len(hcl) not in [4, 8]:
            raise SystemExit('Error: can not parse color `%s`.' % hcl)

        if len(hcl) == 8:
            return int(hcl[4:6], 16), int(hcl[2:4], 16), int(hcl[0:2], 16), int(hcl[6:8], 16)

        return 17 * int(hcl[2],16), 17 * int(hcl[1],16), 17 * int(hcl[0],16), 17 * int(hcl[3],16)

    @staticmethod
    def new_image(img_size: t.ImgSize, hex_color: str = '') -> t.Image:
        image = np.zeros((img_size[1], img_size[0], 4), dtype=np.uint8)
        if hex_color != '':
            image[:,:] = ImgUtils.hex_to_bgra(hex_color)
        return image
