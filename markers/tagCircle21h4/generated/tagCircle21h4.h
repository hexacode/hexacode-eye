#ifndef _TAGCircle21H4
#define _TAGCircle21H4

#include "apriltag.h"

#ifdef __cplusplus
extern "C" {
#endif

apriltag_family_t *tagCircle21h4_create();
void tagCircle21h4_destroy(apriltag_family_t *tf);

#ifdef __cplusplus
}
#endif

#endif
